﻿= Categories =

* [http://groovy.codehaus.org/Groovy+Categories Groovy Categories]
* [http://points-and-edges.blogspot.com/2009/04/groovy-categories-vs-expandometaclass.html Groovy Categories vs ExpandoMetaClass]
* [http://groovyconsole.appspot.com/script/169001 Type coercion with <code>'''CoercionCategory '''</code>]
* [http://groovy.codehaus.org/gapi/groovy/xml/dom/DOMCategory.html <code>'''groovy.xml.dom.DOMCategory'''</code>]
* [http://groovy.codehaus.org/gapi/groovy/time/TimeCategory.html <code>'''groovy.time.TimeCategory'''</code>]
* [[#@Category, @Mixin|<code>'''@Category, @Mixin'''</code>]]


<syntaxhighlight lang="groovy">
class StringCategory { static String   lower(String self)     { self.toLowerCase() }}
class NumberCategory { static Distance getMeters(Number self) { new Distance(number: self) }} // getMeters() => "meters"

use ( StringCategory ) { assert "test" == "TeSt".lower() }
use ( NumberCategory ) { def dist = 300.meters }


use ( groovy.xml.dom.DOMCategory ) {
  assert html.head.title.text() == 'Test'
  assert html.body.p.text()     == 'This is a test.'
  assert html.find{ it.tagName  == 'body' }.tagName == 'body'
  assert html.getElementsByTagName('*').grep{ it.'@class' }.size() == 2
}


use ( groovy.time.TimeCategory ) {
    println 1.minute.from.now
    println 10.hours.ago
    println new Date() - 3.months
}
</syntaxhighlight>


== [http://groovy.codehaus.org/groovy-jdk/ '''Groovy JDK'''] ==

* [http://fisheye.codehaus.org/browse/groovy/trunk/groovy/groovy-core/src/main/org/codehaus/groovy/tools/DgmConverter.java?r=HEAD&content=true <code>'''org.codehaus.groovy.tools.DgmConverter'''</code>]
** <code>'''org.codehaus.groovy.runtime.dgm$832.class'''</code>
** <code>'''META-INF/dgminfo'''</code>
* [http://groovy.codehaus.org/gapi/org/codehaus/groovy/runtime/DefaultGroovyMethods.html <code>'''org.codehaus.groovy.runtime.DefaultGroovyMethods (DGM)'''</code>]
* [http://groovy.codehaus.org/gapi/org/codehaus/groovy/runtime/DefaultGroovyStaticMethods.html <code>'''org.codehaus.groovy.runtime.DefaultGroovyStaticMethods'''</code>]
* [http://groovy.codehaus.org/gapi/org/codehaus/groovy/runtime/DateGroovyMethods.html <code>'''org.codehaus.groovy.runtime.DateGroovyMethods'''</code>]
* [http://groovy.codehaus.org/api/org/codehaus/groovy/runtime/EncodingGroovyMethods.html <code>'''org.codehaus.groovy.runtime.EncodingGroovyMethods'''</code>]
* [http://groovy.codehaus.org/gapi/org/codehaus/groovy/runtime/ProcessGroovyMethods.html <code>'''org.codehaus.groovy.runtime.ProcessGroovyMethods'''</code>] ''(new in <code>'''1.7.5'''</code>)''
* [http://groovy.codehaus.org/gapi/org/codehaus/groovy/runtime/SqlGroovyMethods.html <code>'''org.codehaus.groovy.runtime.SqlGroovyMethods'''</code>]
* [http://groovy.codehaus.org/gapi/org/codehaus/groovy/runtime/SwingGroovyMethods.html <code>'''org.codehaus.groovy.runtime.SwingGroovyMethods'''</code>]
* [http://groovy.codehaus.org/gapi/org/codehaus/groovy/runtime/XmlGroovyMethods.html <code>'''org.codehaus.groovy.runtime.XmlGroovyMethods'''</code>]


= MOP =

* [http://www.ibm.com/developerworks/java/library/j-pg06239.html Practically Groovy: Metaprogramming with closures, ExpandoMetaClass, and categories]
* [http://pinboard.in/u:evgenyg/t:groovy%20mop <code>'''pinboard.in/u:evgenyg/t:groovy mop'''</code>]


== Dynamic Property access / Method Invocation  ==


<syntaxhighlight lang="groovy">
/**
 * Dynamic property access
 */
obj."$property"
obj[(property)]
obj."$property" = value

/**
 * Dynamic method invocation
 */
object."$methodName"( *args )
object.invokeMethod( methodName, *args )


/**
 * Dynamic object creation
 */
def <T> T construct(Class<T> clazz, Map args) { clazz.newInstance(args) }
assert 'groovy.util.Expando' == construct(Expando, [a:'aa']).class.name
assert 'aa'                  == construct(Expando, [a:'aa']).a
</syntaxhighlight>


== MOP Classes ==

* [http://groovy.codehaus.org/api/groovy/lang/GroovyObject.html <code>'''groovy.lang.GroovyObject'''</code>]
* [http://groovy.codehaus.org/api/groovy/lang/MetaObjectProtocol.html <code>'''groovy.lang.MetaObjectProtocol'''</code>]
** [http://groovy.codehaus.org/api/groovy/lang/MetaClass.html <code>'''groovy.lang.MetaClass'''</code>]
*** [http://groovy.codehaus.org/gapi/org/codehaus/groovy/runtime/HandleMetaClass.html <code>'''groovy.runtime.HandleMetaClass'''</code>]
*** [http://groovy.codehaus.org/gapi/groovy/lang/MetaClassImpl.html <code>'''groovy.lang.MetaClassImpl'''</code>] : Allows methods to be dynamically added to existing classes at runtime
**** [http://groovy.codehaus.org/gapi/groovy/lang/ExpandoMetaClass.html <code>'''groovy.lang.ExpandoMetaClass'''</code>] : Allows to dynamically add or replace methods, constructors, and properties using a closure syntax.


== MetaProgramming Hooks ==

* [http://groovy.codehaus.org/Using+invokeMethod+and+getProperty Using invokeMethod and getProperty]
* [http://groovy.codehaus.org/Using+methodMissing+and+propertyMissing Using methodMissing and propertyMissing]
* Implement [http://groovy.codehaus.org/gapi/groovy/lang/GroovyInterceptable.html <code>'''groovy.lang.GroovyInterceptable'''</code>] to intercept invocation of existing methods with <code>'''invokeMethod (String name, args)'''</code>


{| border="1" cellspacing="0" cellpadding="5" class="wikitable"
|-
! Method
! Description
! Usages
|-
| <code>'''getProperty (String property)'''</code>
| Intercepts ''<u>every</u>'' property read access
|
* [http://fisheye.codehaus.org/browse/groovy/trunk/groovy/groovy-core/src/main/groovy/util/Expando.java?r=HEAD&content=true <code>'''groovy.util.Expando'''</code>]
|-
| <code>'''setProperty (String property, Object newValue)'''</code>
| Intercepts ''<u>every</u>'' property write access
|
* [http://fisheye.codehaus.org/browse/groovy/trunk/groovy/groovy-core/src/main/groovy/util/Expando.java?r=HEAD&content=true <code>'''groovy.util.Expando'''</code>]
|-
| <code>'''invokeMethod (String name, args)'''</code>
| Intercepts ''<u>every</u>'' method access
|
* [http://fisheye.codehaus.org/browse/groovy/tags/GROOVY_1_7_5/src/main/groovy/util/BuilderSupport.java?r=HEAD&content=true#l58 <code>'''groovy.util.BuilderSupport.invokeMethod(String name)'''</code>]
* AOP: <code>'''this.metaClass.getMetaMethod(name, args).invoke(this, args)'''</code>
* Builders
* <code>'''StubFor'''</code> / <code>'''MockFor'''</code>
|-
| <code>'''propertyMissing (String name[, value])'''</code>
| Intercepts failed property access.  ''<u>Maximum performance penalty!</u>''
|
* Grails: domain class [http://www.grails.org/DomainClass+Dynamic+Methods#Properties dynamic properties]
|-
| <code>'''methodMissing (String name, args)'''</code>
| Intercepts failed method dispatch. ''<u>Maximum performance penalty!</u>''
|
* [[#Intercept + Cache + Invoke|"Intercept + Cache + Invoke"]]
* Grails: [http://grails.org/GORM+-+Querying dynamic finders], domain class [http://www.grails.org/DomainClass+Dynamic+Methods#Methods dynamic methods]
|}


== Inspecting MetaClass ==

<syntaxhighlight lang="groovy">
String.metaClass.methods.each    { println it.name }
String.metaClass.properties.each { println it.name }

println Foo.metaClass.getMetaMethods()         // List<MetaMethod>
println Foo.metaClass.getProperties()          // List<MetaProperty>
println Foo.metaClass.getMetaMethod( '..' )    // MetaMethod
println Foo.metaClass.getMetaProperty( '..' )  // MetaProperty
println Foo.metaClass.respondsTo ( new Foo(), "bar"  )
println Foo.metaClass.hasProperty( new Foo(), "prop" )
</syntaxhighlight>


* <code>'''respondsTo'''</code> only works for "real" methods and those added via <code>'''ExpandoMetaClass'''</code> and not for cases where <code>'''invokeMethod'''</code> or <code>'''methodMissing'''</code> are overridden. It is impossible in these cases to tell if an object responds to a method without actually invoking the method.


== [http://groovy.codehaus.org/gapi/groovy/util/Expando.html '''groovy.util.Expando'''] ==

* Dynamically expandable JavaScript-like bean.
* [http://groovy.codehaus.org/gapi/groovy/util/Expando.html Groovydoc], [http://fisheye.codehaus.org/browse/groovy/trunk/groovy/groovy-core/src/main/groovy/util/Expando.java?r=HEAD&content=true src]


<syntaxhighlight lang="groovy">
def ex             = new Expando()
ex.propertyName    = 'Some Value'
ex.anotherPropName = 'Another Value'
ex.printDate       = { String header -> println "[$header]: [${ new Date() }]" }

println ex.propertyName
println ex.anotherPropName
ex.printDate( "Date" )
</syntaxhighlight>


== [http://groovy.codehaus.org/gapi/groovy/lang/ExpandoMetaClass.html '''groovy.lang.ExpandoMetaClass'''] ==

* [http://groovy.codehaus.org/gapi/groovy/lang/ExpandoMetaClass.html Groovydoc], [http://fisheye.codehaus.org/browse/groovy/trunk/groovy/groovy-core/src/main/groovy/lang/ExpandoMetaClass.java?r=HEAD&content=true src]
* [http://groovy.codehaus.org/groovy-jdk/java/lang/Class.html#getMetaClass() <code>'''java.lang.Class.getMetaClass()'''</code>]
* Normally, <code>'''SomeClass.metaClass'''</code> is [http://fisheye.codehaus.org/browse/groovy/trunk/groovy/groovy-core/src/main/org/codehaus/groovy/runtime/HandleMetaClass.java?r=HEAD&content=true <code>'''org.codehaus.groovy.runtime.HandleMetaClass'''</code>]
* After <code>'''"p.metaClass.speak = { -> ... }"'''</code> <code>'''HandleMetaClass'''</code> switches the object's metaclass from <code>'''MetaClassImpl'''</code> to <code>'''ExpandoMetaClass'''</code>


<syntaxhighlight lang="groovy">
groovy> println String.metaClass
groovy> String.metaClass.aaa = { "aaa" }
groovy> println String.metaClass

org.codehaus.groovy.runtime.HandleMetaClass@1a503f[groovy.lang.MetaClassImpl@1a503f[class java.lang.String]]
groovy.lang.ExpandoMetaClass@1205d8d[class java.lang.String]
</syntaxhighlight>


* [http://groovy.codehaus.org/ExpandoMetaClass Using ExpandoMetaClass to add behaviour]
* Behaves like an <code>'''Expando'''</code>, allowing the addition or replacement of methods, properties and constructors on the fly.
* By default <code>'''ExpandoMetaClass'''</code> doesn't do inheritance. To enable this you must call [http://groovy.codehaus.org/gapi/groovy/lang/ExpandoMetaClass.html#enableGlobally() <code>'''ExpandoMetaClass.enableGlobally()'''</code>].


<syntaxhighlight lang="groovy">
/**
 * Borrowing methods
 */
Person.metaClass.buyHouse = new MortgageLender().&borrowMoney


/**
 * Adding methods
 */
Number.metaClass.multiply       =  { Amount amount -> amount.times(delegate) }
Number.metaClass.div            =  { Amount amount -> amount.inverse().times(delegate) }
Book.metaClass.titleInUpperCase << {-> title.toUpperCase() } // "Appends" the new method, if the method already exists an exception will be thrown
Book.metaClass.titleInUpperCase =  {-> title.toUpperCase() } // Replaces an instance method

Person.metaClass.whatIsThis     = { String arg -> .. }
Person.metaClass.whatIsThis    << { int arg    -> .. } // Overload
Person.metaClass.whatIsThis     = { int arg    -> .. } // Replace

Number.metaClass {
    multiply { Amount amount ->
               amount.times(delegate) }
    div      { Amount amount ->
               amount.inverse().times(delegate) }
}


/**
 * Adding static methods
 */
Dog.metaClass.static.create = { new Dog() }
Dog.metaClass {
    'static' {
        fqn { delegate.name }
    }
}


/**
 * Adding properties
 */
Dog.metaClass.getBreed = { 'Poodle' }   // new Dog().breed
Book.metaClass.author  = "Stephen King" // Property is mutable and has both a setter and getter. It is stored in a ThreadLocal WeakHashMap, don't expect the value to stick around forever!
Car.metaClass {
    lastAccessed = null
    invokeMethod = { String name, args ->
        ..
        delegate.lastAccessed = new Date()
        ..
    }
}


/**
 * Adding constructor
 */
Dog.metaClass.constructor   = { String name  -> new Dog ( name : name  ) }
Book.metaClass.constructor << { String title -> new Book( title: title ) }
Book.metaClass.constructor  = { new Book() } // StackOverflowError!
Book.metaClass.constructor  = { org.springframework.beans.BeanUtils.instantiateClass(Book) } // Bypass StackOverflowError


/**
 * Dynamic name creation
 */
Person.metaClass."changeNameTo${methodName}" = {-> delegate.name = "Bob" }
classes.findAll { it.name.endsWith( 'Codec' ) }.each { codec ->
    Object.metaClass."encodeAs${codec.name-'Codec'}"   = { codec.newInstance().encode(delegate) } // encodeAsHtml,   encodeAsJavaScript,   etc
    Object.metaClass."decodeFrom${codec.name-'Codec'}" = { codec.newInstance().decode(delegate) } // decodeFromHtml, decodeFromJavaScript, etc
}


/**
 * Overriding MOP hooks
 */
Stuff.metaClass.invokeMethod          = { String name, args       -> .. }
Person.metaClass.getProperty          = { String name             -> .. }
Person.metaClass.setProperty          = { String name, value      -> .. }
Number.metaClass.methodMissing        = { String methodName, args -> .. }
Dog.metaClass.static.methodMissing    = { String methodName, args -> .. }
Dog.metaClass.static.propertyMissing  = { String propertyName     -> .. }
Stuff.metaClass.'static'.invokeMethod = { String name, args       -> .. }


/**
 * Adding methods to interfaces
 * Requires ExpandoMetaClass.enableGlobally()!
 */
List.metaClass.sizeDoubled  = {-> delegate.size() * 2 }
HttpSession.metaClass.getAt = { String key -> delegate.getAttribute(key) }
HttpSession.metaClass.putAt = { String key, Object val -> delegate.setAttribute(key, val) }


/**
 * Adding Methods to instance
 */
def str = "hello test"
def s   = "aaaaaaaaaa"

str.metaClass.test = { "test" }
s.metaClass {
   aaa { "aaa" }
   bbb { "bbb" }
}

assert "test" == str.test()
assert "aaa"  == s.aaa()
assert "bbb"  == s.bbb()


/**
 * Custom casting
 */
String.metaClass.asType = { Long cl -> .. }


/**
 * Undo metaprogramming
 */
String.metaClass = null
</syntaxhighlight>


=== Intercept + Cache + Invoke ===

<syntaxhighlight lang="groovy">
Foo.metaClass.methodMissing = { String name, args ->         // Intercept
    def impl                = { Object[] varArgs  -> ... }
    Foo.metaClass."${name}" = impl                           // Cache
    impl( *args )                                            // Invoke
}
</syntaxhighlight>


== Runtime mixins ==

<syntaxhighlight lang="groovy">
class DivingAbility { def dive() { ... }}
class FlyingAbility { def fly()  { ... }}
class JamesBondVehicle { .. }

JamesBondVehicle.mixin DivingAbility, FlyingAbility
new JamesBondVehicle().dive()
new JamesBondVehicle().fly()
</syntaxhighlight>


= AST Transformations =

* [http://groovy.codehaus.org/Compile-time+Metaprogramming+-+AST+Transformations Compile-time Metaprogramming - AST Transformations]
* [http://groovy.codehaus.org/Groovy+Transforms Groovy Transforms - additional AST Transformations]
* [http://groovy.codehaus.org/gapi/org/codehaus/groovy/ast/package-summary.html <code>'''org.codehaus.groovy.ast'''</code>]
* [http://groovy.codehaus.org/gapi/org/codehaus/groovy/transform/package-summary.html <code>'''org.codehaus.groovy.transform'''</code>]


* [[#@Immutable|<code>'''@Immutable'''</code>]]
* [[#@Delegate|<code>'''@Delegate'''</code>]]
* [[#@Singleton|<code>'''@Singleton'''</code>]]
* [[#@Lazy|<code>'''@Lazy'''</code>]]
* [[#@Newify|<code>'''@Newify'''</code>]]
* [[#@Category, @Mixin|<code>'''@Category'''</code>]]
* [[#@Category, @Mixin|<code>'''@Mixin'''</code>]]
* [[#@PackageScope|<code>'''@PackageScope'''</code>]]
* [[#@Grab|<code>'''@Grab'''</code>]]
* [[#@Bindable, @Vetoable|<code>'''@Bindable'''</code>]]
* [[#@Bindable, @Vetoable|<code>'''@Vetoable'''</code>]]
* <code>'''@Typed'''</code> - [http://code.google.com/p/groovypptest/wiki/Welcome '''Groovy++''']


== [http://groovy.codehaus.org/gapi/groovy/lang/Immutable.html @Immutable] ==

* [http://groovy.codehaus.org/Immutable+AST+Macro Immutable AST Macro]
* Class becomes final, all its fields become final, and you cannot change its state after construction


<syntaxhighlight lang="groovy">
@Immutable final class Punter {
    String first, last
}
@Immutable class Coordinates {
    double lat, lng
}
</syntaxhighlight>


== [http://groovy.codehaus.org/gapi/groovy/lang/Delegate.html @Delegate] ==

* [http://www.ibm.com/developerworks/java/library/j-pg08259.html Practically Groovy: The @Delegate annotation]
* [http://groovy.codehaus.org/Delegate+transformation Delegate transformation]


<syntaxhighlight lang="groovy">
/**
 * The Groovy compiler adds all of Date's methods to the Event class, and those methods simply delegate the call to the Date field.
 */
class Event
{
    @Delegate Date when
    String title, url
}

def df      = new SimpleDateFormat("yyyy/MM/dd")
def gr8conf = new Event(title: "GR8 Conference", url: "http://www.gr8conf.org",       when: df.parse("2009/05/18"))
def javaOne = new Event(title: "JavaOne",        url: "http://java.sun.com/javaone/", when: df.parse("2009/06/02"))

assert gr8conf.before(javaOne.when)

class Employee { def doTheWork() { "done" }}
class Manager  { @Delegate Employee slave = new Employee()}
assert new Manager().doTheWork() == "done"


/**
 * If the delegate is not a final class, it is even possible to make the Event class a subclass of Date simply by extending Date.
 */
class Event extends Date
{
    @Delegate Date when
    String title, url
}


/**
 * In the case you are delegating to an interface you don't need to explictely say you implement the interface of the delegate.
 * The @Delegate annotation fully supports polymorphism by promoting not only the delegate class's methods to the outer class, but the delegate's interfaces as well.
 */
class LockableList
{
    @Delegate                     private List list = []
//  @Delegate(interfaces = false) private List list = []
    @Delegate                     private Lock lock = new ReentrantLock()
}

assert new LockableList() instanceof Lock
assert new LockableList() instanceof List
</syntaxhighlight>


== [http://groovy.codehaus.org/gapi/groovy/lang/Singleton.html @Singleton] ==

* [http://groovy.codehaus.org/Singleton+transformation Singleton transformation]


<syntaxhighlight lang="groovy">
public class T
{
    public static final T instance = new T();
    private T() {}
}

@Singleton class T {}
@Singleton(lazy = true) class T {}

T.instance
</syntaxhighlight>


== [http://groovy.codehaus.org/gapi/groovy/lang/Lazy.html @Lazy] ==

* [http://groovy.codehaus.org/Lazy+transformation Lazy transformation]


<syntaxhighlight lang="groovy">
class Person { @Lazy              List pets = ['Cat', 'Dog', 'Bird']
               @Lazy(soft = true) List pets = ['Cat', 'Dog', 'Bird'] }

class Dude   { @Lazy              List pets = retrieveFromSlowDB()   }
</syntaxhighlight>


== [http://groovy.codehaus.org/gapi/groovy/lang/Newify.html @Newify] ==

* [http://groovy.codehaus.org/Newify+transformation Newify transformation]


<syntaxhighlight lang="groovy">
def buildTree() {
    new Tree(new Tree(new Leaf(1), new Leaf(2)), new Leaf(3))
}

@Newify([Tree, Leaf]) buildTree() {
    Tree(Tree(Leaf(1), Leaf(2)), Leaf(3))
}

@Newify([Coordinates, Path])
def build() {
    Path(
        Coordinates(48.824068, 2.531733),
        Coordinates(48.857840, 2.347212),
        Coordinates(48.858429, 2.342622)
    )
}
</syntaxhighlight>


== [http://groovy.codehaus.org/gapi/groovy/lang/Category.html @Category], [http://groovy.codehaus.org/gapi/groovy/lang/Mixin.html @Mixin] ==

* [http://groovy.codehaus.org/Category+and+Mixin+transformations Category and Mixin transformations]
* [http://groovy.codehaus.org/gapi/groovy/lang/Category.html <code>'''groovy.lang.Category'''</code>]
* Classes conforming to the conventional Groovy category conventions can be used within <code>'''use'''</code> statements or mixed in at compile time with the <code>'''@Mixin'''</code> transformation or at runtime with the <code>'''mixin'''</code> method on classes.
* <code>'''@Category'''</code> only applies to one single type at a time, unlike classical categories which can be applied to any number of types.


<syntaxhighlight lang="groovy">
@Category(Integer)
class IntegerOps { def triple() { this * 3 }}
use ( IntegerOps ) { assert 25.triple() == 75 }

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@Category( A )
class AUtils1 { def triple() { "[$this]".multiply(3) }}

@Category( A )
class AUtils2 { def duplicate() { "[$this]".multiply(2) }}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@Mixin([ AUtils1, AUtils2 ])
@Mixin( AUtils1 )
class A { String toString(){ this.class.name }}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class A { String toString(){ this.class.name }}
A.mixin ([ AUtils1, AUtils2 ])
A.mixin AUtils1

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

assert "[A][A][A]" == new A().triple()
assert "[A][A]"    == new A().duplicate()
</syntaxhighlight>


== [http://groovy.codehaus.org/gapi/groovy/lang/PackageScope.html @PackageScope] ==

* [http://groovy.codehaus.org/PackageScope+transformation PackageScope transformation]
* To be able to expose a field with package-scope visibility, you can annotate your field with the <code>'''@PackageScope'''</code> annotation.


== [http://groovy.codehaus.org/gapi/groovy/lang/Grab.html @Grab] ==

* [http://groovy.codehaus.org/Grape Grape]
* [http://groovy.codehaus.org/Using+Hibernate+with+Groovy Using Hibernate with Groovy]
* Applies to imports, packages, variable declaration


<syntaxhighlight lang="groovy">
@GrabResolver( name='neo4j-public-repo', root='http://m2.neo4j.org' )
@Grab( 'org.neo4j:neo4j-kernel:1.1.1' )
import org.neo4j.kernel.EmbeddedGraphDatabase

@Grab( group='org.ccil.cowan.tagsoup', module='tagsoup', version='0.9.7' )
def getHtml() { ... }

@Grab( 'net.sf.json-lib:json-lib:2.3:jdk15' )
def builder = new net.sf.json.groovy.JsonGroovyBuilder()
</syntaxhighlight>


== [http://groovy.codehaus.org/gapi/groovy/beans/Bindable.html @Bindable], [http://groovy.codehaus.org/gapi/groovy/beans/Vetoable.html @Vetoable] ==

* [http://groovy.codehaus.org/Bindable+and+Vetoable+transformation Bindable and Vetoable transformation]


== AstBuilder ==

* [http://groovy.codehaus.org/Global+AST+Transformations Global AST Transformations]
* [http://groovy.codehaus.org/Local+AST+Transformations Local AST Transformations]
** [http://groovy.codehaus.org/api/org/codehaus/groovy/transform/ASTTransformation.html <code>'''org.codehaus.groovy.transform.ASTTransformation'''</code>]
** [http://groovy.codehaus.org/gapi/org/codehaus/groovy/control/SourceUnit.html#getAST() <code>'''org.codehaus.groovy.control.SourceUnit.getAST()'''</code>]
** [http://groovy.codehaus.org/gapi/org/codehaus/groovy/ast/ModuleNode.html <code>'''org.codehaus.groovy.ast.ModuleNode'''</code>]
** [http://groovy.codehaus.org/gapi/org/codehaus/groovy/ast/MethodNode.html#setCode(org.codehaus.groovy.ast.stmt.Statement) <code>'''org.codehaus.groovy.ast.MethodNode.setCode(Statement)'''</code>]
** [http://groovy.codehaus.org/gapi/org/codehaus/groovy/ast/stmt/BlockStatement.html <code>'''org.codehaus.groovy.ast.stmt.BlockStatement'''</code>]
** [http://groovy.codehaus.org/gapi/org/codehaus/groovy/ast/builder/AstBuilder.html <code>'''org.codehaus.groovy.ast.builder.AstBuilder'''</code>]
* [http://groovy.codehaus.org/Building+AST+Guide Building AST Guide]
* [http://groovy.codehaus.org/Compiler+Phase+Guide Compiler Phase Guide]
* [http://groovyastbrowser.appspot.com/ Groovy AST Browser]
* <code>'''Groovy Console -> Script -> Inspect Ast (Ctrl+T)'''</code>


<syntaxhighlight lang="groovy">
@WithLogging
def greet() {
  println "Hello World"
}

greet()

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@Retention(RetentionPolicy.SOURCE)
@Target([ElementType.METHOD])
@GroovyASTTransformationClass(["gep.LoggingASTTransformation"])
public @interface WithLogging {}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@GroovyASTTransformation(phase=CompilePhase.SEMANTIC_ANALYSIS)
public class LoggingASTTransformation implements ASTTransformation
{

  public void visit(ASTNode[] nodes, SourceUnit sourceUnit)
  {
      List methods = sourceUnit.getAST()?.getMethods()
      // find all methods annotated with @WithLogging
      methods.findAll { MethodNode method -> method.getAnnotations(new ClassNode(WithLogging)) }.each
      {
          MethodNode method ->
          ...
      }
  }
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

new AstBuilder().buildFromString(''' "Hello" ''')

new AstBuilder().buildFromCode { "Hello" }

List<ASTNode> nodes = new AstBuilder().buildFromSpec {
    block {
        returnStatement {
            constant "Hello"
        }
    }
}
</syntaxhighlight>


= Testing =

* [http://martinfowler.com/articles/mocksArentStubs.html Mocks Aren't Stubs]
* '''CUT''' = Class Under Test


== Unit Tests ==

* [http://groovy.codehaus.org/gapi/groovy/util/GroovyTestCase.html <code>'''groovy.util.GroovyTestCase'''</code>]
* [http://groovy.codehaus.org/gapi/groovy/lang/GroovyLogTestCase.html <code>'''groovy.lang.GroovyLogTestCase'''</code>]


<syntaxhighlight lang="groovy">
import static groovy.util.GroovyTestCase.*

class MyClass
{
    @Test
    void getAnything()
    {
       ...
       shouldFail( UnsupportedOperationException ){ badList << "will this work?" }
       ...
    }
}
</syntaxhighlight>


== Stubs ==

* Intercepts all method calls to instances of a given class and returns a predefined result
* [http://groovy.codehaus.org/gapi/groovy/mock/interceptor/StubFor.html <code>'''groovy.mock.interceptor.StubFor'''</code>]


<syntaxhighlight lang="groovy">
/**
 * Stub Person calls
 */
class Person { String first, last }

def stub = new StubFor(Person)

stub.demand.with {
   getFirst{ 'dummy' }
   getLast { 'name'  }
}

stub.use {
   def john = new Person(first:'John', last:'Smith')
   assert john.first == 'dummy'
   assert john.last  == 'name'
}

/**
 * Expect demanded call cardinalities to match demanded ranges.
 * Assert that all demanded method calls happened.
 */
stub.expect.verify()


/**
 * Demand calls to different methods
 */
stub.demand.getFirst{ 'dummy' }
stub.demand.getLast { 'name'  }


/**
 * When calls to the stubbed method should yield different results per call, add the respective demands in sequence
 */
stub.demand.getFirst { 'dummy' }
stub.demand.getFirst { 'ddddd' }


/**
 * Provide a range to specify how often the demanded closure should apply; the default is (1..1)
 */
stub.demand.getFirst(0..35) { 'dummy' }


/**
 * It is possible to react to the method arguments that CUT passes
 */
stub.demand.methodOne {
   number -> ( assert 0 == number % 2 )
   return 1
}
</syntaxhighlight>


== Mocks ==

* Strict expectation of a mock verifies that all the demanded method calls happen in exactly the sequence of the specification.
* The first method call that breaks this sequence causes the test to fail immediately.
* There is no need to explicitly call the verify method, because that happens by default when the <code>'''use{ .. }'''</code> closure ends.
* What gets asserted is whether the CUT follows a specified protocol when talking with the outside world. A protocol defines the rules that the CUT has to obey when calling the collaborator.
* [http://groovy.codehaus.org/gapi/groovy/mock/interceptor/MockFor.html <code>'''groovy.mock.interceptor.MockFor'''</code>]


<syntaxhighlight lang="groovy">
def relay(request) {
    def farm = new SortableFarm()
    farm.sort()
    farm.getMachines()[0].send(request)
}

def farmMock = new MockFor(SortableFarm)
farmMock.demand.sort(){}
farmMock.demand.getMachines { [new Expando( send: {} )] }
farmMock.use { relay(null) }
</syntaxhighlight>
