<div style="float:right; margin-top:2%; margin-right: 2%;">
''"Fly away, Stanley. Be free!"''
</div>


= Introduction =

<code>'''"ivy-maven-plugin"'''</code> widens Maven boundaries and brings Ivy repositories to your Maven builds! It allows to perform 2 major actions:

* <u>Use [http://ant.apache.org/ivy/history/latest-milestone/settings/resolvers.html '''Ivy resolvers'''] to retrieve artifacts from repositories '''non-compatible with Maven'''</u>, such as those of [http://confluence.jetbrains.net/display/TCD7/Artifact+Dependencies#ArtifactDependencies-ConfiguringArtifactDependenciesUsingAntBuildScript TeamCity], [http://bazaar.launchpad.net/~gradle-plugins/gradle-release/trunk/view/head:/installation/apply.groovy Launchpad] or [http://ant.apache.org/ivy/history/latest-milestone/resolver/filesystem.html local file system].<br/>''This is useful if your Maven build needs to access artifacts available online or locally in some kind of non-Maven storage for which a custom resolver can be defined''.

* <u>Add Ivy artifacts to Maven scope such as <code>'''"compile"'''</code> or <code>'''"test"'''</code> and use them in the corresponding compilation or testing phases</u>.<br/>''This is useful if your Maven build needs to compile sources or run tests using libraries not available in Maven repositories.''


Ivy artifacts can be specified in two ways:

* Standard <code>'''"ivy.xml"'''</code> file.
* Standard Maven <code>'''<dependencies>'''</code>.


= Details =

{| border="1" cellspacing="0" cellpadding="5" class="wikitable"
|-
!
! Provided By
!
|-
| align="center" | '''Mailing List'''
| align="center" | [http://nabble.com/ Nabble]
|
* [http://maven-plugins.994461.n3.nabble.com <code>'''maven-plugins.994461.n3.nabble.com'''</code>]
|-
| align="center" | '''Source Code'''
| align="center" | [http://github.com/ GitHub]
|
* [http://github.com/evgeny-goldin/maven-plugins/tree/master/ivy-maven-plugin <code>'''github.com/evgeny-goldin/maven-plugins/tree/master/ivy-maven-plugin'''</code>]
|-
| align="center" | '''License'''
| align="center" |
|
* [http://github.com/evgeny-goldin/maven-plugins/blob/master/license.txt <code>'''Apache License, Version 2.0'''</code>]
|-
| align="center" | '''Tests'''
| align="center" | [http://github.com/ GitHub]
|
* [http://github.com/evgeny-goldin/maven-plugins-test/tree/master/ivy-maven-plugin <code>'''github.com/evgeny-goldin/maven-plugins-test/tree/master/ivy-maven-plugin'''</code>]
|-
| align="center" | '''GroovyDoc'''
| align="center" | [http://docs.codehaus.org/display/GROOVY/The+groovydoc+Ant+task <code>'''<groovydoc>'''</code>]
|
* [http://evgeny-goldin.org/groovydoc/maven-plugins/0.2.5/ <code>'''evgeny-goldin.org/groovydoc/maven-plugins/0.2.5'''</code>]
* [http://evgeny-goldin.org/groovydoc/maven-plugins/0.2.5/com/github/goldin/plugins/ivy/IvyMojo.html <code>'''IvyMojo'''</code>]
|-
| align="center" | '''Issue Tracker'''
| align="center" | [http://www.jetbrains.com/youtrack/ YouTrack]
|
* [http://evgeny-goldin.org/youtrack/issues/pl?q=%23ivy-maven-plugin+ <code>'''#ivy-maven-plugin'''</code>]
* Issues addressed in version: [http://evgeny-goldin.org/youtrack/releaseNotes/pl?q=%23ivy-maven-plugin+%230.2.3.8+%23Resolved+ <code><strong>0.2.3.8</strong></code>]
|-
| align="center" | '''Build Server'''
| align="center" | [http://maven.apache.org/ Maven]<br/>[http://www.jetbrains.com/teamcity/ TeamCity]
|
* [http://evgeny-goldin.org/teamcity/project.html?guest=1&projectId=project2 <code>'''maven-plugins'''</code>]
* [http://evgeny-goldin.org/teamcity/project.html?guest=1&projectId=project3 <code>'''maven-plugins-test'''</code>]
|-
| align="center" | '''Maven Coordinates'''
|
|
* <code>'''com.github.goldin:ivy-maven-plugin:0.2.5'''</code>
|-
| align="center" | '''Goal'''
|
|
* <code>'''ivy'''</code>
|-
| align="center" | '''Default Phase'''
|
|
* <code>'''initialize'''</code>
|-
| align="center" | '''Maven Repository'''
| align="center" | [http://www.jfrog.com Artifactory Online]
|
* [http://evgenyg.artifactoryonline.com/evgenyg/repo/com/github/goldin/ <code>'''evgenyg.artifactoryonline.com/evgenyg/repo/'''</code>]
* [http://search.maven.org/#search|ga|1|com.github.goldin Maven Central]
|}


= Examples =


== Downloading Intellij IDEA Ivy artifacts - Maven dependencies ==


Let's say you need to download Intellij IDEA Community Edition artifacts, available in the publicly available [http://teamcity.jetbrains.com/viewType.html?guest=1&tab=buildTypeStatusDiv&buildTypeId=bt343 TeamCity build], similarly to how it can be done [http://makandracards.com/evgeny-goldin/5477-getting-intellij-idea-jars-using-ivy-and-teamcity with Ant].

'''1.''' Define <code>'''"ivyconf.xml"'''</code> with TeamCity-specific resolver:

<syntaxhighlight lang="xml">
<ivysettings>
    <property name='ivy.checksums' value=''/>
    <statuses>
        <status name='integration' integration='true'/>
    </statuses>
    <resolvers>
        <url name='idea-repo'   alwaysCheckExactRevision='yes' checkmodified='true'>
            <ivy      pattern='http://teamcity.jetbrains.com/guestAuth/repository/download/[module]/[revision]/teamcity-ivy.xml'/>
            <artifact pattern='http://teamcity.jetbrains.com/guestAuth/repository/download/[module]/[revision]/[artifact](.[ext])'/>
        </url>
    </resolvers>
    <modules>
        <module organisation='org' name='bt343' matcher='regexp' resolver='idea-repo'/>
    </modules>
</ivysettings>
</syntaxhighlight>

'''2.''' Plug it in and download IDEA artifacts, specifying them explicitly together with any other Maven dependencies, similarly to Ivy's [http://ant.apache.org/ivy/history/latest-milestone/use/retrieve.html <code>'''<retrieve>'''</code>] ''(taken from [http://github.com/evgeny-goldin/maven-plugins-test/blob/9ac5acc3ae81c6c0078966a0cf493c4112604a1e/ivy-maven-plugin/pom.xml#L65 this test])'':

<syntaxhighlight lang="xml">
<plugin>
    <groupId>com.github.goldin</groupId>
    <artifactId>ivy-maven-plugin</artifactId>
    <version>0.2.5</version>
    <executions>
        <execution>
            <id>get-idea-artifacts</id>
            <goals>
                <goal>ivy</goal>
            </goals>
            <phase>generate-resources</phase>
            <configuration>
                <ivyconf>${project.basedir}/src/main/resources/ivyconf.xml</ivyconf>
                <dir>${outputDir}/idea</dir>
                <dependencies>
                    <dependency>
                        <groupId>ivy.org</groupId>
                        <artifactId>bt343</artifactId>
                        <version>latest.lastSuccessful</version>
                        <classifier>core/intellij-core</classifier>
                        <destFileName>intellij-core.jar</destFileName>
                    </dependency>
                    <dependency>
                        <groupId>org.apache.ant</groupId>
                        <artifactId>ant</artifactId>
                        <version>1.8.2</version>
                    </dependency>
                </dependencies>
            </configuration>
        </execution>
    </executions>
</plugin>
</syntaxhighlight>

Note:

* You could also use [[copy-maven-plugin|<code>'''"copy-maven-plugin"'''</code>]] or [http://maven.apache.org/plugins/maven-dependency-plugin/ <code>'''"maven-dependency-plugin"'''</code>] to download artifacts as if they were regular Maven dependencies but it is not required, as <code>'''"ivy-maven-plugin"'''</code> provides a convenient <code>'''<dir>'''</code> configuration.
* Ivy dependency <code>'''<groupId>'''</code> should start with <code>'''"ivy."'''</code> to distinguish it from regular Maven dependency.
* See [http://confluence.jetbrains.net/display/TCD7/Artifact+Dependencies#ArtifactDependencies-ConfiguringArtifactDependenciesUsingAntBuildScript additional information] about TeamCity acting as Ivy repository and various <code>'''<version>'''</code> values you may use such as <code>'''"latest.lastFinished"'''</code>, <code>'''"latest.lastSuccessful"'''</code>, and <code>'''"latest.lastPinned"'''</code>.
* Ivy dependency <code>'''<classifier>'''</code> plays a role of Ivy [http://ant.apache.org/ivy/history/latest-milestone/ivyfile/dependency-include.html <code>'''<include>'''</code>] element with <code>'''"exact"'''</code> [http://ant.apache.org/ivy/history/latest-milestone/concept.html#matcher pattern matcher] to match a single file. It should be used when multiple artifacts with identical <code>'''"org:name:rev"'''</code> (<code>'''"groupId:artifactId:version"'''</code>) coordinates are available. For example, [http://teamcity.jetbrains.com/viewType.html?guest=1&tab=buildTypeStatusDiv&buildTypeId=bt343 IDEA build] produces multiple artifacts ''(<code>"sources.zip"</code>, <code>"core/intellij-core.jar"</code>, etc)'', all having <code>'''"org:bt343:latest.lastSuccessful"'''</code> coordinates so you need to specify a <code>'''<classifier>'''</code> and a <code>'''<type>'''</code> where required to select a single artifact:


<syntaxhighlight lang="xml">
<dependency>
    <groupId>ivy.org</groupId>
    <artifactId>bt343</artifactId>
    <version>31</version>
    <classifier>sources</classifier>
    <type>zip</type>
</dependency>
<dependency>
    <groupId>ivy.org</groupId>
    <artifactId>bt343</artifactId>
    <version>latest.lastSuccessful</version>
    <classifier>core/intellij-core</classifier>
</dependency>
</syntaxhighlight>



== Downloading Intellij IDEA Ivy artifacts - Ivy dependencies ==


Alternatively, you may specify dependencies you need using <code>'''"ivy.xml"'''</code>.

'''1.''' Define <code>'''"ivyconf.xml"'''</code> with TeamCity-specific resolver, as previously.<br/>
'''2.''' Define <code>'''"ivy.xml"'''</code> with IDEA-specific dependencies or any other dependencies you may need.

<syntaxhighlight lang="xml">
<ivy-module version="1.3">
    <info organisation="com.jetbrains" module="idea"/>
    <dependencies>
        <dependency org="org" name="bt343" rev="latest.lastSuccessful">
            <include name="core/.*" ext="jar" matcher="exactOrRegexp"/>
        </dependency>
    </dependencies>
</ivy-module>
</syntaxhighlight>

'''2.''' Plug them in and download all dependencies defined in <code>'''"ivy.xml"'''</code>, similarly to Ivy's [http://ant.apache.org/ivy/history/latest-milestone/use/retrieve.html <code>'''<retrieve>'''</code>] ''(taken from [http://github.com/evgeny-goldin/maven-plugins-test/blob/9ac5acc3ae81c6c0078966a0cf493c4112604a1e/ivy-maven-plugin/pom.xml#L50 this test])'':

<syntaxhighlight lang="xml">
<plugin>
    <groupId>com.github.goldin</groupId>
    <artifactId>ivy-maven-plugin</artifactId>
    <version>0.2.5</version>
    <executions>
        <execution>
            <id>get-idea-artifacts</id>
            <goals>
                <goal>ivy</goal>
            </goals>
            <phase>generate-resources</phase>
            <configuration>
                <ivyconf>${project.basedir}/src/main/resources/ivyconf.xml</ivyconf>
                <ivy>${project.basedir}/src/main/resources/ivy.xml</ivy>
                <dir>${outputDir}/idea</dir>
            </configuration>
        </execution>
    </executions>
</plugin>
</syntaxhighlight>


== Adding Ivy artifacts to Maven "compile" scope ==


Let's say you want to develop an IDEA plugin and you need <code>'''"intellij-core.jar"'''</code> to be added to the compilation classpath, similarly to Ivy's [http://ant.apache.org/ivy/history/latest-milestone/use/cachepath.html <code>'''<cachepath>'''</code>]. All you need to do is replace <code>'''<dir>'''</code> we've seen before with <code>'''<scope>compile</scope>'''</code> ''(taken from [http://github.com/evgeny-goldin/maven-plugins-test/blob/9ac5acc3ae81c6c0078966a0cf493c4112604a1e/ivy-maven-plugin/pom.xml#L35 this test])'':

<syntaxhighlight lang="xml">
<plugin>
    <groupId>com.github.goldin</groupId>
    <artifactId>ivy-maven-plugin</artifactId>
    <version>0.2.5</version>
    <executions>
        <execution>
            <id>add-idea-artifacts-compile-scope</id>
            <goals>
                <goal>ivy</goal>
            </goals>
            <phase>generate-resources</phase>
            <configuration>
                <ivyconf>${project.basedir}/src/main/resources/ivyconf.xml</ivyconf>
                <ivy>${project.basedir}/src/main/resources/ivy.xml</ivy>
                <scope>compile</scope>
            </configuration>
        </execution>
    </executions>
</plugin>
</syntaxhighlight>

* Make sure you run this execution ''before'' <code>'''"compile"'''</code> phase so that artifacts resolved are added to the compilation classpath before it begins.


= <configuration> =

{| border="1" cellspacing="0" cellpadding="5" class="wikitable" style="width:100%"
|-
! style="width:15%" | Name
! style="width:10%" | Optional
! style="width:75%" | Description
|-
| align="center" | <code>'''<ivyconf>'''</code>
| align="center" | <code>'''false'''</code>
| Ivy settings file path. Local path and resource URL retrieved with [http://docs.oracle.com/javase/6/docs/api/java/lang/ClassLoader.html#getResource(java.lang.String) <code>'''ClassLoader.getResource()'''</code>] is accepted.
|-
| align="center" | <code>'''<ivy>'''</code>
| align="center" | <code>'''true'''</code>
| Ivy dependencies file path. Local path and resource URL retrieved with [http://docs.oracle.com/javase/6/docs/api/java/lang/ClassLoader.html#getResource(java.lang.String) <code>'''ClassLoader.getResource()'''</code>] is accepted.
|-
| align="center" | <code>'''<dependencies>'''</code>
| align="center" | <code>'''true'''</code>
| <code>'''<ivy>'''</code> alternative - dependencies to resolve. Each <code>'''<dependency>'''</code> is an instance of [http://maven.apache.org/plugins/maven-dependency-plugin/apidocs/org/apache/maven/plugin/dependency/fromConfiguration/ArtifactItem.html <code>'''ArtifactItem'''</code>].
|-
| align="center" | <code>'''<scope>'''</code>
| align="center" | <code>'''true'''</code>
| Comma-separated Maven scopes to add artifacts resolved to, similarly to Ivy's [http://ant.apache.org/ivy/history/latest-milestone/use/cachepath.html <code>'''<cachepath>'''</code>].<br/>
<code>'''"plugin-runtime"'''</code> is a special scope, normally used programmatically. Using it adds artifacts to caller's runtime classloader.
|-
| align="center" | <code>'''<dir>'''</code>
| align="center" | <code>'''true'''</code>
| Local directory to copy artifacts resolved to, similarly to Ivy's [http://ant.apache.org/ivy/history/latest-milestone/use/retrieve.html <code>'''<retrieve>'''</code>].
|-
| align="center" | <code>'''<verbose>'''</code>
| align="center" | <code>'''true'''</code>
| Whether Ivy-related actibity should be logged verbosely.<br/>
Only download processes are logged when <code>'''<verbose>'''</code> is set to <code>'''false'''</code>, general Ivy information is logged when it is not specified and verbose logging is provided when it is set to <code>'''true'''</code>.
|-
| align="center" | <code>'''<failOnError>'''</code>
| align="center" | <code>'''true'''</code>
| Whether execution should fail if resolving artifacts didn't succeed, <code>'''true'''</code> by default.
|}