= Introduction =

There are situation when we need '''e-mails to be sent as result of our build process'''. While CI servers like Hudson already send e-mails with jobs status, occasionally, we may need something more. We may want to get a detailed results of our integration tests by mail. Or some special kind of compilation or build process to be sent to another group. Something that is more detailed and much more informative than a simple "Build passed/failed" message from a CI server.

This plugin allows you to send emails from Maven via SMTP server:


<syntaxhighlight lang="xml">
<plugin>
    <groupId>com.github.goldin</groupId>
    <artifactId>mail-maven-plugin</artifactId>
    <version>0.2.5</version>
    <executions>
        <execution>
            <phase>install</phase>
            <goals>
                <goal>send</goal>
            </goals>
            <configuration>
                <smtp>smtp.server</smtp>
                <from>your@from.email</from>
                <mails>
                    <to>some@email;some@other.email</to>
                </mails>
                <subject>This is subject</subject>
                <text>And this is body</text>
                <files>
                    <file>fileToAttach.txt</file>
                    <file>anotherFileToAttach.txt</file>
                </files>
            </configuration>
        </execution>
    </executions>
</plugin>
</syntaxhighlight>


= Details =

{| border="1" cellspacing="0" cellpadding="5" class="wikitable"
|-
!
! Provided By
!
|-
| align="center" | '''Mailing List'''
| align="center" | [http://nabble.com/ Nabble]
|
* [http://maven-plugins.994461.n3.nabble.com <code>'''maven-plugins.994461.n3.nabble.com'''</code>]
|-
| align="center" | '''Source Code'''
| align="center" | [http://github.com/ GitHub]
|
* [http://github.com/evgeny-goldin/maven-plugins/tree/master/mail-maven-plugin <code>'''github.com/evgeny-goldin/maven-plugins/tree/master/mail-maven-plugin'''</code>]
|-
| align="center" | '''License'''
| align="center" |
|
* [http://github.com/evgeny-goldin/maven-plugins/blob/master/license.txt <code>'''Apache License, Version 2.0'''</code>]
|-
| align="center" | '''GroovyDoc'''
| align="center" | [http://docs.codehaus.org/display/GROOVY/The+groovydoc+Ant+task <code>'''<groovydoc>'''</code>]
|
* [http://evgeny-goldin.org/groovydoc/maven-plugins/0.2.5/ <code>'''evgeny-goldin.org/groovydoc/maven-plugins/0.2.5'''</code>]
* [http://evgeny-goldin.org/groovydoc/maven-plugins/0.2.5/com/github/goldin/plugins/mail/MailMojo.html <code>'''MailMojo'''</code>]
|-
| align="center" | '''Issue Tracker'''
| align="center" | [http://www.jetbrains.com/youtrack/ YouTrack]
|
* [http://evgeny-goldin.org/youtrack/issues/pl?q=%23mail-maven-plugin+ <code>'''#mail-maven-plugin'''</code>]
|-
| align="center" | '''Build Server'''
| align="center" | [http://maven.apache.org/ Maven]<br/>[http://www.jetbrains.com/teamcity/ TeamCity]
|
* [http://evgeny-goldin.org/teamcity/project.html?guest=1&projectId=project2 <code>'''maven-plugins'''</code>]
* [http://evgeny-goldin.org/teamcity/project.html?guest=1&projectId=project3 <code>'''maven-plugins-test'''</code>]
|-
| align="center" | '''Maven Coordinates'''
|
|
* <code>'''com.github.goldin:mail-maven-plugin:0.2.5'''</code>
|-
| align="center" | '''Goal'''
|
|
* <code>'''send'''</code>
|-
| align="center" | '''Default Phase'''
|
|
* <code>'''install'''</code>
|-
| align="center" | '''Maven Repository'''
| align="center" | [http://www.jfrog.com Artifactory Online]
|
* [http://evgenyg.artifactoryonline.com/evgenyg/repo/com/github/goldin/ <code>'''evgenyg.artifactoryonline.com/evgenyg/repo/'''</code>]
* [http://search.maven.org/#search|ga|1|com.github.goldin Maven Central]
|}


= <configuration> =

{| border="1" cellspacing="0" cellpadding="5"
|-
| <code>'''<runIf>'''</code>
| Optional. Specifies if plugin needs to be run, see <code>'''"copy-maven-plugin"'''</code> [[copy-maven-plugin#.3CrunIf.3E|documentation]] for more details
|-
| <code>'''<smtp>'''</code>
| Specifies your SMTP server, assumes there's no SMTP Authentication
|-
| <code>'''<from>'''</code>
| Specifies your "From" address
|-
| <code>'''<mails>'''</code>
| Specifies addresses to send e-mails to. <code>'''<to>'''</code>, <code>'''<cc>'''</code>, and <code>'''<bcc>'''</code> nested tags are supported
|-
|
<code>'''<to>'''</code>

<code>'''<cc>'''</code>

<code>'''<bcc>'''</code>
| <code>''';'''</code>-separated list of "To", "Cc", and "Bcc" addresses.

Full names can be specified as <code>'''"Name &amp;lt;mail@address&amp;gt;"'''</code>:

<syntaxhighlight lang="xml">
<to>
    FirstName SecondName &lt;some@email&gt;;
    FirstName SecondName &lt;some@other.email&gt;;
    ...
</to>
<cc>
    some@email;some@other.email;and.another@email
</cc>
<bcc>
    FirstName SecondName &lt;some@email&gt;;
    FirstName SecondName &lt;some@other.email&gt;;
    ...
</bcc>
</syntaxhighlight>

|-
| <code>'''<subject>'''</code>
| Specifies message subject, empty by default
|-
| <code>'''<text>'''</code>
| Specifies message body, empty by default
|-
| <code>'''<textFile>'''</code>
| Specifies message body as a text file, appended to <code>'''<body>'''</code>:

<syntaxhighlight lang="xml">
<textFile>../somewhere/body.txt</textFile>
</syntaxhighlight>

|-
|<code>'''<files>'''</code>
| Specifies files to attach to the message:

<syntaxhighlight lang="xml">
<files>
    <file>../some/file.txt</file>
    <file>../some/other/file.txt</file>
</files>
</syntaxhighlight>

|}


== Dynamic content: <subject>, <text> ==

It is easy to send e-mails with static <code>'''<subject>'''</code> or <code>'''<text>'''</code> known in advance. But what if they dynamic and depend on previous steps? In this case we need to create a corresponding Maven properties before <code>'''"mail-maven-plugin"'''</code> is run and make it use them:


<syntaxhighlight lang="xml">
<configuration>
    <smtp>${smtp.server}</smtp>
    <from>${from}</from>
    <mails>
        <to>${cm.mails}</to>
    </mails>
    <subject>${mail.subject}</subject>
    <text>${mail.body}</text>
    <textFile>${mail.bodyFile}</textFile>
    <files>
        <file>${status.file}</file>
    </files>
</configuration>
</syntaxhighlight>


The idea of creating new properties dynamically is borrowed from Ant where lot's of its tasks [http://ant-contrib.sourceforge.net/tasks/tasks/propertyregex.html create new Ant properties] as a result of their run. Those new properties are later used for different purposes.

Same idea applies here:

# You run something that creates new Maven properties. It can be done with [[properties-maven-plugin|<code>'''"properties-maven-plugin"'''</code>]].
# You run <code>'''"mail-maven-plugin"'''</code> that uses those properties.


== Dynamic content: <textFile>, <files> ==

For <code>'''<textFile>'''</code> and <code>'''<files>'''</code> you may use files whose content is created ''dynamically'' by attaching a file like <code>'''"${project.build.outputDirectory}/status.txt"'''</code> and making sure it contains the relevant content when <code>'''"mail-maven-plugin"'''</code> is run. Right now it is not possible to alter <code>'''<files>'''</code> list dynamically.
