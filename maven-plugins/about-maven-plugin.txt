﻿= Introduction =

<code>'''"about-maven-plugin"'''</code> adds textual "about" file to build artifacts created, linking each binary artifact to the build  environment and sources it was created with.

The file added contains details about build date and time, last commit made in the project, Java / Maven versions used for the build and other optional elements like project
dependencies, local paths, Maven, system and environment variables. If the project was built by Jenkins, TeamCity or Hudson build server then corresponding job details
are also added to the file generated.

For example, this [http://evgenyg.artifactoryonline.com/evgenyg/plugins-releases-local/com/github/goldin/about-maven-plugin/0.2.4/about-maven-plugin-0.2.4.jar '''jar file''']
contains the following data in <code>'''"about~com.github.goldin~about-maven-plugin~0.2.4.txt"'''</code>:

<syntaxhighlight lang="text">
 Created with http://evgeny-goldin.com/wiki/about-maven-plugin, version "0.2.3.8"
===============================================================================
 TeamCity Info
===============================================================================
 Server         : [http://evgeny-goldin.org/teamcity/]
 Job            : [http://evgeny-goldin.org/teamcity/viewLog.html?buildId=6194]
 Log            : [http://evgeny-goldin.org/teamcity/viewLog.html?buildId=6194&tab=buildLog]
 Server Version : [7.1 (build 23265)]
 Project        : [maven-plugins]
 Configuration  : [Build, Deploy]
 Build Number   : [857]
===============================================================================
 Git Info
===============================================================================
 Repositories   : [origin	git@github.com:evgeny-goldin/maven-plugins.git (fetch)
                   origin	git@github.com:evgeny-goldin/maven-plugins.git (push)]
 Branch         : [master]
 Git Status     : [# On branch master
                   nothing to commit (working directory clean)]
 Commit         : [eb8f3cd][eb8f3cd689921d25571d480be2fb065a4ef3fa79]
 Commit Date    : [Thu, 10 May 2012 22:24:03 +0200]
 Commit Author  : [Evgeny Goldin <evgenyg@gmail.com>]
 Commit Message : [<version>0.2.4</version>
                   <gcommons-version>0.5.4</gcommons-version>]
===============================================================================
 Build Info
===============================================================================
 Host           : [evgeny-goldin.org]
 Build Time     : Started         - [10 May, Thursday, 2012, 23:42:11 (Israel Daylight Time:GMT+0300)]
 Build Time     : "About" created - [10 May, Thursday, 2012, 23:43:17 (Israel Daylight Time:GMT+0300)]
 User           : [evgenyg]
 Java           : [1.6.0_30][Sun Microsystems Inc.][Java HotSpot(TM) 64-Bit Server VM]
 OS             : [Linux][amd64][2.6.35-32-server]
===============================================================================
 Maven Info
===============================================================================
 MAVEN_OPTS     : [-Xmx512m -XX:MaxPermSize=256m]
 Version        : [3.0.4]
 Project        : [MavenProject: com.github.goldin:about-maven-plugin:0.2.4]
 Goals          : [clean, source:jar, install]
 Name           : [com.github.goldin:about-maven-plugin:0.2.4]
 Coordinates    : [com.github.goldin:about-maven-plugin:0.2.4]
 Dependencies   : [com.github.goldin:about-maven-plugin:maven-plugin:0.2.4
                   +- com.github.goldin:maven-common:jar:0.2.4:compile
                   |  +- org.apache.maven:maven-core:jar:3.0.4:compile
                   |  |  +- org.apache.maven:maven-settings:jar:3.0.4:compile
                   |  |  +- org.apache.maven:maven-settings-builder:jar:3.0.4:compile
                   ...
                   |  +- org.sonatype.sisu:sisu-inject-bean:jar:2.3.0:compile
                   |  +- org.jfrog.maven.annomojo:maven-plugin-anno:jar:1.4.1:compile
                   |  \- com.jcraft:jsch:jar:0.1.48:compile
                   \- junit:junit:jar:4.10:compile]
===============================================================================
 Maven Properties
===============================================================================
 [aether-version]          :[1.13.1]
 [annomojo-version]        :[1.4.1]
 [ant-version]             :[1.8.3]
 [gcommons-version]        :[0.5.4]
 [gcontracts-version]      :[1.2.5]
 [gmaven-version]          :[1.4]
 [groovy-version]          :[1.8.6]
 [java-version]            :[1.6]
 [junit-version]           :[4.10]
 [maven-version]           :[3.0.4]
 [previous-plugins-version]:[0.2.3.8]
 [spring-batch-version]    :[2.1.8.RELEASE]
 [spring-version]          :[3.1.1.RELEASE]
===============================================================================
</syntaxhighlight>


This file was created by [http://github.com/evgeny-goldin/maven-plugins/blob/410a57cb5d3f8289045c3a57869ba4a61451e065/pom.xml#L389 providing] the second example from the following section.


= Examples =


== Updating existing archives ==

First example configures the plugin to add an <code>'''"about"'''</code> file to <code>'''"META-INF"'''</code> of all <code>'''"*.jar"'''</code> and <code>'''"*.zip"'''</code> archives found in <code>'''"${project.build.directory}"'''</code>. The following data will be stored in it:<br/>
* SCM details: last Git commit / SVN revision, its message and author.
* Local build paths.
* Maven properties.
* Maven dependencies.
* System properties.
* Environment variables.

The plugin is also configured not to fail if any error occurs or no archive files are found.

<syntaxhighlight lang="xml">
<plugin>
    <groupId>com.github.goldin</groupId>
    <artifactId>about-maven-plugin</artifactId>
    <version>0.2.5</version>
    <executions>
        <execution>
            <id>create-about</id>
            <goals>
                <goal>create-about</goal>
            </goals>
            <phase>verify</phase>
            <configuration>
                <include>*.jar, *.zip</include>
                <dumpSCM>true</dumpSCM>
                <dumpPaths>true</dumpPaths>
                <dumpMaven>true</dumpMaven>
                <dumpDependencies>true</dumpDependencies>
                <dumpSystem>true</dumpSystem>
                <dumpEnv>true</dumpEnv>
                <failOnError>false</failOnError>
                <failIfNotFound>false</failIfNotFound>
            </configuration>
        </execution>
    </executions>
</plugin>
</syntaxhighlight>


== Creating "about" files in a predefined location without updating any archives ==

It is possible not to update existing archives, which may be time-consuming. You can instruct the plugin to merely create <code>'''"about"'''</code> files in a predefined location so they're archived together with the rest of your data. The second example below shows one such configuration - the plugin only runs if <code>'''"${project.build.outputDirectory}"'''</code> directory exists and it creates the <code>'''"about"'''</code> file in <code>'''"${project.build.outputDirectory}"'''</code> so it'll be packed together with any project artifacts. It dumps Maven properties and dependencies but doesn't dump system properties, environment variables or any local paths for security reasons. In fact, that's exactly how <code>'''"about"'''</code> files are [http://github.com/evgeny-goldin/maven-plugins/blob/410a57cb5d3f8289045c3a57869ba4a61451e065/pom.xml#L389 added] to all my Maven plugins.


<syntaxhighlight lang="xml">
<plugin>
    <groupId>com.github.goldin</groupId>
    <artifactId>about-maven-plugin</artifactId>
    <version>0.2.5</version>
    <executions>
        <execution>
            <id>copy-about</id>
            <phase>prepare-package</phase>
            <goals>
                <goal>create-about</goal>
            </goals>
            <configuration>
                <runIf>{{ new File( project.build.outputDirectory ).directory }}</runIf>
                <dumpDependencies>true</dumpDependencies>
                <dumpMaven>true</dumpMaven>
                <fileName>${project.build.outputDirectory}/about~${project.groupId}~${project.artifactId}~${project.version}.txt</fileName>
                <updateArchives>false</updateArchives>
            </configuration>
        </execution>
    </executions>
</plugin>
</syntaxhighlight>



= Details =

{| border="1" cellspacing="0" cellpadding="5" class="wikitable"
|-
!
! Provided By
!
|-
| align="center" | '''Mailing List'''
| align="center" | [http://nabble.com/ Nabble]
|
* [http://maven-plugins.994461.n3.nabble.com <code>'''maven-plugins.994461.n3.nabble.com'''</code>]
|-
| align="center" | '''Source Code'''
| align="center" | [http://github.com/ GitHub]
|
* [http://github.com/evgeny-goldin/maven-plugins/tree/master/about-maven-plugin <code>'''github.com/evgeny-goldin/maven-plugins/tree/master/about-maven-plugin'''</code>]
|-
| align="center" | '''License'''
| align="center" |
|
* [http://github.com/evgeny-goldin/maven-plugins/blob/master/license.txt <code>'''Apache License, Version 2.0'''</code>]
|-
| align="center" | '''Tests'''
| align="center" | [http://github.com/ GitHub]
|
* [http://github.com/evgeny-goldin/maven-plugins-test/tree/master/about-maven-plugin <code>'''github.com/evgeny-goldin/maven-plugins-test/tree/master/about-maven-plugin'''</code>]
|-
| align="center" | '''GroovyDoc'''
| align="center" | [http://docs.codehaus.org/display/GROOVY/The+groovydoc+Ant+task <code>'''<groovydoc>'''</code>]
|
* [http://evgeny-goldin.org/groovydoc/maven-plugins/0.2.5/ <code>'''evgeny-goldin.org/groovydoc/maven-plugins/0.2.5'''</code>]
* [http://evgeny-goldin.org/groovydoc/maven-plugins/0.2.5/com/github/goldin/plugins/about/AboutMojo.html <code>'''AboutMojo'''</code>]
|-
| align="center" | '''Issue Tracker'''
| align="center" | [http://www.jetbrains.com/youtrack/ YouTrack]
|
* [http://evgeny-goldin.org/youtrack/issues/pl?q=%23about-maven-plugin+ <code>'''#about-maven-plugin'''</code>]
|-
| align="center" | '''Build Server'''
| align="center" | [http://maven.apache.org/ Maven]<br/>[http://www.jetbrains.com/teamcity/ TeamCity]
|
* [http://evgeny-goldin.org/teamcity/project.html?guest=1&projectId=project2 <code>'''maven-plugins'''</code>]
* [http://evgeny-goldin.org/teamcity/project.html?guest=1&projectId=project3 <code>'''maven-plugins-test'''</code>]
|-
| align="center" | '''Maven Coordinates'''
|
|
* <code>'''com.github.goldin:about-maven-plugin:0.2.5'''</code>
|-
| align="center" | '''Goal'''
|
|
* <code>'''create-about'''</code>
|-
| align="center" | '''Default Phase'''
|
|
* <code>'''package'''</code>
|-
| align="center" | '''Maven Repository'''
| align="center" | [http://www.jfrog.com Artifactory Online]
|
* [http://evgenyg.artifactoryonline.com/evgenyg/repo/com/github/goldin/ <code>'''evgenyg.artifactoryonline.com/evgenyg/repo/'''</code>]
* [http://search.maven.org/#search|ga|1|com.github.goldin Maven Central]
|}


= <configuration> =



{| border="1" cellspacing="0" cellpadding="5" class="wikitable" style="width:100%"
|-
! style="width:10%" | Name
! style="width:15%" | Default value
! style="width:75%" | Description
|-
| align="center" | <code>'''<runIf>'''</code>
| align="center" |
| Controls whether or not plugin should be executed, accepts a [[copy-maven-plugin#Groovy_.22extension_points.22|Groovy expression]].
|-
| align="center" | <code>'''<updateArchives>'''</code>
| align="center" | <code>'''"true"'''</code>
| Controls whether or not plugin should update an existing archives or just create the <code>'''"about"'''</code> file.
|-
| align="center" | <code>'''<prefix>'''</code>
| align="center" | <code>'''"META-INF"'''</code>
| Archive directory where <code>'''"about"'''</code> file is created. Use <code>'''"<prefix>/</prefix>"'''</code> if <code>'''"about"'''</code> file should be added to archive's root.
|-
| align="center" | <code>'''<fileName>'''</code>
| align="center" | <code>'''"about-${project.groupId}-${project.artifactId}-${project.version}.txt"'''</code>
| Name of the file created.
* If <code>'''"<updateArchives>"'''</code> is <code>'''"true"'''</code> (default value) then <code>'''"about"'''</code> file is added as <code>'''"<prefix>/<fileName>"'''</code> to every archive.
* If <code>'''"<updateArchives>"'''</code> is <code>'''"false"'''</code> then <code>'''"about"'''</code> file is created as:
** <code>'''"${project.build.outputDirectory}/<fileName>"'''</code> if it's a relative path
** <code>'''"<fileName>"'''</code> if it's an absolute path
|-
| align="center" | <code>'''<dumpSCM>'''</code>
| align="center" | <code>'''"true"'''</code>
| Whether SCM details should be added to <code>'''"about"'''</code> file.

* Git and SVN systems are recognized automatically but their command-line clients are expected to be installed on a build machine (this will be changed in [http://evgeny-goldin.org/youtrack/issue/pl-411 future versions]).
* Make sure the corresponding <code>'''".git"'''</code> and <code>'''".svn"'''</code> folders are available when your project is built.<br/>In TeamCity it requires <code>'''"VCS checkout mode"'''</code> to be <code>'''"Automatically on agent"'''</code> in <code>'''"Version Control Settings"'''</code>.
|-
| align="center" | <code>'''<dumpMaven>'''</code>
| align="center" | <code>'''"false"'''</code>
| Whether Maven properties should be added to <code>'''"about"'''</code> file.
|-
| align="center" | <code>'''<dumpEnv>'''</code>
| align="center" | <code>'''"false"'''</code>
| Whether environment variables should be added to <code>'''"about"'''</code> file.
|-
| align="center" | <code>'''<dumpSystem>'''</code>
| align="center" | <code>'''"false"'''</code>
| Whether Java system properties should be added to <code>'''"about"'''</code> file.
|-
| align="center" | <code>'''<dumpPaths>'''</code>
| align="center" | <code>'''"false"'''</code>
| Whether various build paths (Maven home, build directory, etc) should be added to <code>'''"about"'''</code> file.
|-
| align="center" | <code>'''<dumpDependencies>'''</code>
| align="center" | <code>'''"false"'''</code>
| Whether project transitive dependencies, result of running <code>'''"mvn dependency:tree"'''</code>, should be added to <code>'''"about"'''</code> file.
|-
| align="center" | <code>'''<addContent>'''</code>
| align="center" |
| Arbitrary content to add to <code>'''"about"'''</code> file, [[copy-maven-plugin#Groovy_.22extension_points.22|Groovy expression]] is supported.
|-
| align="center" | <code>'''<endOfLine>'''</code>
| align="center" | <code>'''"windows"'''</code>
| If <code>'''"windows"'''</code> - end of line in the file created is <code>'''"\r\n"'''</code>. For any other value it is <code>'''"\n"'''</code>.
|-
| align="center" | <code>'''<directory>'''</code>
| align="center" | <code>'''"${project.build.directory}"'''</code>
| Directory where archives to update are located.
|-
| align="center" | <code>'''<include>'''</code>
| align="center" | <code>'''"*.jar"'''</code>
| Comma-separated list of archive patterns to update.
|-
| align="center" | <code>'''<exclude>'''</code>
| align="center" |
| Comma-separated list of archive patterns to exclude from update.
|-
| align="center" | <code>'''<failOnError>'''</code>
| align="center" | <code>'''"true"'''</code>
| Whether build execution should fail if plugin execution fails for any reason.

You may want to specify <code>'''"false"'''</code> if build stability is of high importance ''(in other words, you don't care if anything went wrong with <code>'''"about"'''</code> file as long as the build keeps running)''.
|-
| align="center" | <code>'''<failIfNotFound>'''</code>
| align="center" | <code>'''"true"'''</code>
| Whether build execution should fail if no archives were found in <code>'''"<directory>"'''</code> matching the <code>'''"<include>"'''</code> pattern.
|}