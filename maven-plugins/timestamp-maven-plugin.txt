= Introduction =

Historically, there were no easy ways in Maven to set a <code>'''${timestamp}'''</code> property so <code>'''"timestamp-maven-plugin"'''</code> was born as another fix to the problem. But before you use it, please have a look at [[Properties-maven-plugin|<code>'''properties-maven-plugin'''</code>]] which is capable of creating any Maven properties with Groovy snippets.


<syntaxhighlight lang="xml">
<plugin>
    <groupId>com.github.goldin</groupId>
    <artifactId>timestamp-maven-plugin</artifactId>
    <version>0.2.5</version>
    <executions>
        <execution>
            <id>set-build-timestamp</id>
            <goals>
                <goal>timestamp</goal>
            </goals>
            <phase>validate</phase>
            <configuration>
                <timestamp>
                    <property>timestamp</property>
                    <pattern>MMM dd, yyyy (HH:mm)</pattern>
                    <timezone>GMT+2</timezone>
                    <locale>UK</locale>
                </timestamp>
            </configuration>
        </execution>
    </executions>
</plugin>
</syntaxhighlight>


= Details =

{| border="1" cellspacing="0" cellpadding="5" class="wikitable"
|-
!
! Provided By
!
|-
| align="center" | '''Mailing List'''
| align="center" | [http://nabble.com/ Nabble]
|
* [http://maven-plugins.994461.n3.nabble.com <code>'''maven-plugins.994461.n3.nabble.com'''</code>]
|-
| align="center" | '''Source Code'''
| align="center" | [http://github.com/ GitHub]
|
* [http://github.com/evgeny-goldin/maven-plugins/tree/master/timestamp-maven-plugin <code>'''github.com/evgeny-goldin/maven-plugins/tree/master/timestamp-maven-plugin'''</code>]
|-
| align="center" | '''License'''
| align="center" |
|
* [http://github.com/evgeny-goldin/maven-plugins/blob/master/license.txt <code>'''Apache License, Version 2.0'''</code>]
|-
| align="center" | '''Tests'''
| align="center" | [http://github.com/ GitHub]
|
* [http://github.com/evgeny-goldin/maven-plugins-test/tree/master/timestamp-maven-plugin <code>'''github.com/evgeny-goldin/maven-plugins-test/tree/master/timestamp-maven-plugin'''</code>]
|-
| align="center" | '''GroovyDoc'''
| align="center" | [http://docs.codehaus.org/display/GROOVY/The+groovydoc+Ant+task <code>'''<groovydoc>'''</code>]
|
* [http://evgeny-goldin.org/groovydoc/maven-plugins/0.2.5/ <code>'''evgeny-goldin.org/groovydoc/maven-plugins/0.2.5'''</code>]
* [http://evgeny-goldin.org/groovydoc/maven-plugins/0.2.5/com/github/goldin/plugins/timestamp/TimestampMojo.html <code>'''TimestampMojo'''</code>]
|-
| align="center" | '''Issue Tracker'''
| align="center" | [http://www.jetbrains.com/youtrack/ YouTrack]
|
* [http://evgeny-goldin.org/youtrack/issues/pl?q=%23timestamp-maven-plugin+ <code>'''#timestamp-maven-plugin'''</code>]
|-
| align="center" | '''Build Server'''
| align="center" | [http://maven.apache.org/ Maven]<br/>[http://www.jetbrains.com/teamcity/ TeamCity]
|
* [http://evgeny-goldin.org/teamcity/project.html?guest=1&projectId=project2 <code>'''maven-plugins'''</code>]
* [http://evgeny-goldin.org/teamcity/project.html?guest=1&projectId=project3 <code>'''maven-plugins-test'''</code>]
|-
| align="center" | '''Maven Coordinates'''
|
|
* <code>'''com.github.goldin:timestamp-maven-plugin:0.2.5'''</code>
|-
| align="center" | '''Goal'''
|
|
* <code>'''timestamp'''</code>
|-
| align="center" | '''Default Phase'''
|
|
* <code>'''validate'''</code>
|-
| align="center" | '''Maven Repository'''
| align="center" | [http://www.jfrog.com Artifactory Online]
|
* [http://evgenyg.artifactoryonline.com/evgenyg/repo/com/github/goldin/ <code>'''evgenyg.artifactoryonline.com/evgenyg/repo/'''</code>]
* [http://search.maven.org/#search|ga|1|com.github.goldin Maven Central]
|}


= <configuration> =

You can define a single <code>'''<timestamp>'''</code>:


<syntaxhighlight lang="xml">
<configuration>
    <runIf>{{ .. }}</runIf>
    <timestamp>
        <property>build-timestamp</property>
        <pattern>MMM dd, yyyy (HH:mm)</pattern>
    </timestamp>
</configuration>
</syntaxhighlight>


.. or multiple <code>'''<timestamps>'''</code>:


<syntaxhighlight lang="xml">
<configuration>
    <runIf>{{ .. }}</runIf>
    <timestamps>
        <timestamp>
            <property>build-timestamp-date</property>
            <pattern>MMM dd, yyyy</pattern>
        </timestamp>
        <timestamp>
            <property>build-timestamp-time</property>
            <pattern>HH:mm</pattern>
        </timestamp>
    </timestamps>
</configuration>
</syntaxhighlight>


* <code>'''<runIf>'''</code> is optional. It specifies if plugin needs to be run, see <code>'''"copy-maven-plugin"'''</code> [[copy-maven-plugin#.3CrunIf.3E|documentation]] for more details.
* Each <code>'''<timestamp>'''</code> can have <code>'''<timezone>'''</code> and <code>'''<locale>'''</code> specified:


<syntaxhighlight lang="xml">
<configuration>
    <timestamp>
        <property>build-timestamp</property>
        <pattern>MMM dd, yyyy (HH:mm)</pattern>
        <timezone>EST</timezone>
        <locale>en_US</locale>
    </timestamp>
</configuration>
</syntaxhighlight>


* [http://download.oracle.com/javase/6/docs/api/java/text/SimpleDateFormat.html <code>'''SimpleDateFormat'''</code>] documentation provides <code>'''<pattern>'''</code> format reference
* [http://download.oracle.com/javase/6/docs/api/java/util/TimeZone.html <code>'''TimeZone'''</code>] possible values can be seen in [http://www.vmware.com/support/developer/vc-sdk/visdk400pubs/ReferenceGuide/timezone.html this table].
* [http://download.oracle.com/javase/6/docs/api/java/util/Locale.html <code>'''Locale'''</code>] possible values can be seen in [http://www.loc.gov/standards/iso639-2/php/English_list.php this table].


= Groovy =

When not specified explicitly <code>'''new Date()'''</code> is used for taking a timestamp. Sometimes "now" isn't good enough and for those cases, <code>'''<time>'''</code> [[copy-maven-plugin#Groovy_.22extension_points.22|Groovy snippet]] can be used to specify another <code>'''Date'''</code>, [http://groovy.codehaus.org/groovy-jdk/java/util/Date.html#plus(int) relative] to "now" or [http://groovy.codehaus.org/groovy-jdk/java/util/Date.html#parse(java.lang.String,%20java.lang.String) parsed] from another <code>'''String'''</code>:


<syntaxhighlight lang="xml">
<configuration>
    <!-- "now" + 7 days -->
    <time>{{ new Date().plus( 7 ) }}</time>
    <timestamp>
        <property>future-timestamp</property>
        <pattern>MMM dd, yyyy (HH:mm)</pattern>
    </timestamp>
</configuration>

<configuration>
    <!-- "Aug 10, 2009" => "10/08/09" -->
    <time>{{ Date.parse( "MMM dd, yyyy", "Aug 10, 2009" ) }}</time>
    <timestamp>
        <property>timestamp</property>
        <pattern>dd/MM/yy</pattern>
    </timestamp>
</configuration>

<properties>
    <format>MMM dd, yyyy</format>
    <time>Aug 10, 2009</time>
</properties>

<configuration>
    <!-- "Aug 10, 2009" => "10/08/09" -->
    <time>{{ Date.parse( format, time ) }}</time>
    <timestamp>
        <property>timestamp</property>
        <pattern>dd/MM/yy</pattern>
    </timestamp>
</configuration>
</syntaxhighlight>


* Snippet's return value is expected to be an instance of <code>'''java.util.Date'''</code>.
* [[copy-maven-plugin#Maven_properties_as_Groovy_variables|Maven properties]] are available as Groovy variables.
