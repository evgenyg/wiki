<div style="float:right">
[[File:Crawler.png]]
</div>


= Introduction =

This plugin scans web resources for broken links giving a great deal of configuration and programmatic control (using Groovy callbacks) over the scanning process. Usual scanners provide a lot of false alarms without letting one specify which "broken links" can be safely ignored, something that is done easily using this plugin.


= Example =


* [http://github.com/evgeny-goldin/gradle-plugins-test/blob/master/crawlers.gradle <code>'''crawlers.gradle'''</code>]


This wiki is scanned [http://evgeny-goldin.org/teamcity/viewType.html?buildTypeId=bt86&guest=1 periodically] for broken links using the [http://github.com/evgeny-goldin/gradle-plugins-test/blob/149fbbc50995d66d64902736ab1a53123d81ead1/crawlers.gradle#L72 following configuration]:


<syntaxhighlight lang="groovy">
apply plugin: 'crawler'

..

buildscript {
    repositories { jcenter() }
    dependencies { classpath 'com.github.goldin.plugins.gradle:crawler:0.3' }
}

..

task wikiCrawler( type: com.github.goldin.plugins.gradle.crawler.CrawlerTask ) {

    final ignoredLinksEndsWith = 'redirect=no /wiki/... /wiki/MyPage'.tokenize()
    final ignoredLinksContains = 'Talk: Special: printable= oldid= action='.tokenize()

    config { it.with {

            baseUrl            = 'evgeny-goldin.com/wiki'
            teamcityMessages   = true
            checkExternalLinks = true
            displayLinksPath   = true

            log                = file( 'log-wiki.txt' )
            linksMapFile       = file( 'links-wiki.txt' )
            newLinksMapFile    = file( 'newLinks-wiki.txt' )

            minimumLinks       = 1100
            minimumBytes       = 510 * 1024

            linkTransformers   = [{ String link -> link.replaceFirst( /evgeny-goldin.org(:\d+)?/, 'evgeny-goldin.org:8148' ) }]
            ignoredLinks       = [{ String link -> ignoredLinksEndsWith.any { link.endsWith( it )}},
                                  { String link -> ignoredLinksContains.any { link.contains( it )}}]
    }}
}</syntaxhighlight>


Many other crawlers can be seen in [http://github.com/evgeny-goldin/gradle-plugins-test/blob/master/crawlers.gradle <code>'''crawlers.gradle'''</code>] and their results [http://evgeny-goldin.org/teamcity/project.html?projectId=Crawlers&guest=1 in TeamCity].


= Details =


{| border="1" cellspacing="0" cellpadding="5" class="wikitable" style="width:60%"
| style="width:15%" align="center" | '''Tasks'''
| style="width:85%" |
* <code>''''crawler''''</code> - scans the resource specified for broken links
|-
| align="center" | '''Source Code'''
|
* [http://github.com/evgeny-goldin/gradle-plugins <code>'''github.com/evgeny-goldin/gradle-plugins'''</code>]
|-
| align="center" | '''License'''
|
* [http://github.com/evgeny-goldin/gradle-plugins/blob/master/license.txt <code>'''Apache License, Version 2.0'''</code>]
|-
| align="center" | '''GroovyDoc'''
|
* [http://evgeny-goldin.org/groovydoc/gradle-plugins/0.3/ <code>'''evgeny-goldin.org/groovydoc/gradle-plugins/0.3'''</code>]
|-
| align="center" | '''Tests'''
|
* [http://github.com/evgeny-goldin/gradle-plugins-test <code>'''github.com/evgeny-goldin/gradle-plugins-test'''</code>]
|-
| align="center" | '''Issue Tracker'''
|
* [http://evgeny-goldin.org/youtrack/issues/gp?q=%23{%27crawler%27+plugin}+ <code>'''#{'crawler' plugin}'''</code>]
|-
| align="center" | '''Build Server'''
|
* [http://evgeny-goldin.org/teamcity/project.html?guest=1&projectId=gradleplugins <code>'''gradle-plugins'''</code>]
|-
| align="center" | '''Repositories''',<br/>'''Coordinates'''
|
* [http://jcenter.bintray.com/com/github/goldin/plugins/gradle/crawler/0.3/ JCenter]
* [http://search.maven.org/#artifactdetails%7Ccom.github.goldin.plugins.gradle%7Ccrawler%7C0.3%7Cjar Maven Central]

<syntaxhighlight lang="groovy">
buildscript {
    repositories { jcenter() }
    dependencies { classpath 'com.github.goldin.plugins.gradle:crawler:0.3' }
}
</syntaxhighlight>
|}


= crawler { .. } extension properties =


* [http://evgeny-goldin.org/groovydoc/gradle-plugins/0.3/com/github/goldin/plugins/gradle/crawler/CrawlerExtension.html <code>'''com.github.goldin.plugins.gradle.crawler.CrawlerExtension'''</code>]


{| border="1" cellspacing="0" cellpadding="5" class="wikitable" style="width:100%"
|-
! style="width:5%"  | Name
! style="width:5%"  | Type
! style="width:20%" | Default value
! style="width:70%" | Description
|-
| align="center" | <code>'''baseUrl'''</code>
| align="center" | <code>'''String'''</code>
| align="center" |
| Resource URL to start the crawling process from, <code>'''"http://"'''</code> can be omitted.
|-
| align="center" | <code>'''rootLinks'''</code>
| align="center" | <code>'''List<String>'''</code>
| align="center" | <code>'''[]'''</code>
| Root links to start the crawling process from if <code>'''baseUrl'''</code> is not specified.
|-
| align="center" | <code>'''checkExternalLinks'''</code>
| align="center" | <code>'''boolean'''</code>
| align="center" | <code>'''false'''</code>
| Whether external links (links that point to other sites) are checked.
|- valign="top"
| align="center" | <code>'''userAgent'''</code>
| align="center" | <code>'''String'''</code>
| align="center" | <code>'''"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11"'''</code>
| <code>'''"User-Agent"'''</code> header to send during the scanning process.
|-
| align="center" | <code>'''threadPoolSize'''</code>
| align="center" | <code>'''int'''</code>
| align="center" | <code>'''Runtime.runtime.availableProcessors()'''</code>
| Number of threads to use for the scanning process.
|-
| align="center" | <code>'''connectTimeout'''</code>
| align="center" | <code>'''int'''</code>
| align="center" | <code>'''15000'''</code>
| Connection timeout (in milliseconds).
|-
| align="center" | <code>'''readTimeout'''</code>
| align="center" | <code>'''int'''</code>
| align="center" | <code>'''15000'''</code>
| Read timeout (in milliseconds).
|-
| align="center" | <code>'''minimumLinks'''</code>
| align="center" | <code>'''int'''</code>
| align="center" |
| Minimal amount of links that should be found during the scanning process. Fails the build if the scanning process does too little work.
|-
| align="center" | <code>'''minimumBytes'''</code>
| align="center" | <code>'''long'''</code>
| align="center" |
| Minimal amount of bytes that should be downloaded during the scanning process. Fails the build if the scanning process downloads too few data.
|-
| align="center" | <code>'''retries'''</code>
| align="center" | <code>'''int'''</code>
| align="center" | <code>'''3'''</code>
| Number of check attempts before the link is decalred as "broken".
|-
| align="center" | <code>'''retryDelay'''</code>
| align="center" | <code>'''long'''</code>
| align="center" | <code>'''5000'''</code>
| Delay between retries (in milliseconds).
|-
| align="center" | <code>'''retryStatusCodes'''</code>
| align="center" | <code>'''List<Integer>'''</code>
| align="center" | <code>'''[ 500 ]'''</code>
| Error status codes allowing to re-check the link.
|-
| align="center" | <code>'''retryExceptions'''</code>
| align="center" | <code>'''List<Class>'''</code>
| align="center" | <code>'''[ SocketTimeoutException ]'''</code>
| Connection exceptions allowing to re-check the link.
|-
| align="center" | <code>'''maxDepth'''</code>
| align="center" | <code>'''int'''</code>
| align="center" | <code>'''Integer.MAX_VALUE'''</code>
| Maximal scanning depth starting from the first page.
|-
| align="center" | <code>'''pageDownloadLimit'''</code>
| align="center" | <code>'''long'''</code>
| align="center" | <code>'''102400'''</code> (100Kb)
| Maximal size for a single page (in bytes). Allows to prevent resources that are too big from being scanned or being treated by mistake as HTML pages.
|-
| align="center" | <code>'''totalDownloadLimit'''</code>
| align="center" | <code>'''long'''</code>
| align="center" | <code>'''104857600'''</code> (100Mb)
| Maximal total download size during the scanning process (in bytes).
|-
| align="center" | <code>'''requestDelay'''</code>
| align="center" | <code>'''long'''</code>
| align="center" | <code>'''0'''</code>
| Delay before making each request (in milliseconds).
|-
| align="center" | <code>'''log'''</code>
| align="center" | <code>'''File'''</code>
| align="center" |
| File to write the scanning progress to.
|-
| align="center" | <code>'''linksMapFile'''</code>
| align="center" | <code>'''File'''</code>
| align="center" |
| File to write links as they're found, for every page.
|-
| align="center" | <code>'''newLinksMapFile'''</code>
| align="center" | <code>'''File'''</code>
| align="center" |
| File to write ''new'' links as they're found, for every page.
|- valign="top"
| align="center" | <code>'''nonHtmlExtensions'''</code>
| align="center" | <code>'''List<String>'''</code>
| align="center" | <code>'''css js ico logo gif jpg jpeg png ps eps doc pdf zip jar war ear hpi rar gz xml xlsx xsd xsl svg flv swf mp4 mp3 avi mkv'''</code>
| Extensions of resources that are checked for availability by sending <code>'''HEAD'''</code> requests and are not downloaded.
|-
| align="center" | <code>'''htmlExtensions'''</code>
| align="center" | <code>'''List<String>'''</code>
| align="center" |
| Extensions of resources that are always treated as HTML resources and therefore downloaded.
|-
| align="center" | <code>'''linkTransformers'''</code>
| align="center" | <code>'''List<Closure>'''</code>
| align="center" |
| Callbacks allowing to transform links found during the scanning process.
* Input: <code>'''String'''</code> - page URL.
* Output: <code>'''String'''</code> - page URL transformed.
|-
| align="center" | <code>'''pageTransformers'''</code>
| align="center" | <code>'''List<Closure>'''</code>
| align="center" |
| Callbacks allowing to transform pages found during the scanning process.
* Input: <code>'''String'''</code> - page URL, <code>'''String'''</code> - page content.
* Output: <code>'''String'''</code> - transformed page content.
|-
| align="center" | <code>'''nonHtmlLinks'''</code>
| align="center" | <code>'''List<Closure>'''</code>
| align="center" |
| Callbacks allowing to determine non-HTML resources.
* Input: <code>'''String'''</code> - page URL.
* Output: <code>'''boolean'''</code> - whether URL specified belongs to '''non-HTML''' resource so it doesn't need to be downloaded.
|-
| align="center" | <code>'''ignoredLinks'''</code>
| align="center" | <code>'''List<Closure>'''</code>
| align="center" |
| Callbacks allowing to exclude certain links from the scanning process.
* Input: <code>'''String'''</code> - page URL.
* Output: <code>'''boolean'''</code> - whether URL specified should be ignored.
|-
| align="center" | <code>'''ignoredContent'''</code>
| align="center" | <code>'''List<Closure>'''</code>
| align="center" |
| Callbacks allowing to exclude certain pages from the scanning process.
* Input: <code>'''String'''</code> - page URL, <code>'''String'''</code> - page content.
* Output: <code>'''boolean'''</code> - whether page specified should be ignored.
|-
| align="center" | <code>'''verifyContent'''</code>
| align="center" | <code>'''List<Closure>'''</code>
| align="center" |
| Callbacks allowing to verify every page found during the scanning process.
* Input: <code>'''String'''</code> - page URL, <code>'''String'''</code> - page content.
* Output: <code>'''boolean'''</code> or no result - whether page specified verifies successfully. If <code>'''false'''</code> value is returned by the callback the crawling process is aborted immediately.
|-
| align="center" | <code>'''ignoredBrokenLinks'''</code>
| align="center" | <code>'''List<Closure>'''</code>
| align="center" |
| Callbacks allowing to specify which broken links can be ignored.
* Input: <code>'''String'''</code> - page URL.
* Output: <code>'''boolean'''</code> - whether URL specified of a broken link can be ignored.
|-
| align="center" | <code>'''zipLogFiles'''</code>
| align="center" | <code>'''boolean'''</code>
| align="center" | <code>'''true'''</code>
| Whether log files produced are [http://evgeny-goldin.org/teamcity/viewLog.html?buildTypeId=bt86&buildId=lastSuccessful&tab=artifacts&guest=1 zipped].
|-
| align="center" | <code>'''replaceSpecialCharacters'''</code>
| align="center" | <code>'''boolean'''</code>
| align="center" | <code>'''true'''</code>
| Whether HTML special characters (<code>'''%3A'''</code>, <code>'''%2F'''</code>, <code>'''&amp;lt;'''</code>, etc) should be replaced.
|-
| align="center" | <code>'''removeHtmlComments'''</code>
| align="center" | <code>'''boolean'''</code>
| align="center" | <code>'''true'''</code>
| Whether HTML comments should be removed from pages content before scanning for links.
|-
| align="center" | <code>'''displaySummary'''</code>
| align="center" | <code>'''boolean'''</code>
| align="center" | <code>'''true'''</code>
| Whether a summary is printed when the scanning process finishes.
|-
| align="center" | <code>'''displayLinks'''</code>
| align="center" | <code>'''boolean'''</code>
| align="center" | <code>'''false'''</code>
| Whether links found are printed when the scanning process finishes.
|-
| align="center" | <code>'''displayLinksPath'''</code>
| align="center" | <code>'''boolean'''</code>
| align="center" | <code>'''false'''</code>
| Whether for each broken link its path stating from the first page is printed.
|-
| align="center" | <code>'''failOnFailure'''</code>
| align="center" | <code>'''boolean'''</code>
| align="center" | <code>'''true'''</code>
| Whether the scanning process is aborted if checking any page fails for any reason.
|-
| align="center" | <code>'''failOnBrokenLinks'''</code>
| align="center" | <code>'''boolean'''</code>
| align="center" | <code>'''false'''</code>
| Whether the build should fail if broken links found. Normally, the build only displays a number of broken links found.
|-
| align="center" | <code>'''teamcityMessages'''</code>
| align="center" | <code>'''boolean'''</code>
| align="center" | <code>'''false'''</code>
| Whether TeamCity [http://confluence.jetbrains.com/display/TCD7/Build+Script+Interaction+with+TeamCity#BuildScriptInteractionwithTeamCity-ServiceMessages service messages] are used to report the scanning progress and the overall result, as in the screenshot below.
|}


[[File:Crawler-TeamCity.png|link=http://evgeny-goldin.org/teamcity/project.html?projectId=Crawlers&guest=1]]


= Having multiple crawlers in the same project =


Having a single resource to scann allows to use a default <code>'''crawler'''</code> extension per project:


<syntaxhighlight lang="groovy">
crawler {
    baseUrl = ' .. '
    ...
}
</syntaxhighlight>


However, if there is a need to scan multiple different resources the following task definition can be used:


<syntaxhighlight lang="groovy">
task resourceName( type: com.github.goldin.plugins.gradle.crawler.CrawlerTask ) {
    config { it.with {

            baseUrl = ' .. '
            ...
    }}
}

task otherResourceName( type: com.github.goldin.plugins.gradle.crawler.CrawlerTask ) {
    config { it.with {

            baseUrl = ' .. '
            ...
    }}
}
</syntaxhighlight>


See [http://github.com/evgeny-goldin/gradle-plugins-test/blob/master/crawlers.gradle <code>'''crawlers.gradle'''</code>] for additional examples.


= Troubleshooting =

Running Gradle with <code>'''"-i"'''</code> flag prints out a scanning progress to the console:


<syntaxhighlight lang="xml">
./gradlew -i -b crawlers.gradle wikiCrawler

...

:wikiCrawler
Task ':wikiCrawler' has not declared any outputs, assuming that it is out-of-date.
----------------------------------------------------------------------------
 Checking [evgeny-goldin.com/wiki] (208.113.162.253) links with [8] threads
 Root link:
 * [http://evgeny-goldin.com/wiki]
----------------------------------------------------------------------------

[http://evgeny-goldin.com/wiki] - sending GET request ..
[http://evgeny-goldin.com/wiki] - redirected to [http://evgeny-goldin.com/wiki/Main_Page], [1446] ms
[http://evgeny-goldin.com/wiki/Main_Page] - sending GET request ..
##teamcity[progressMessage '0 links processed, 0 broken, 0 queued']
[http://evgeny-goldin.com/wiki/Main_Page] - [6899 => 17301] bytes, [4186] ms
[http://evgeny-goldin.com/wiki/Main_Page] - depth [0], 42 links found, 41 new, 1 processed, 0 queued:
* [http://evgeny-goldin.com/]
* [http://evgeny-goldin.com/blog/]
* [http://evgeny-goldin.com/blog/personal-productivity/]
* [http://evgeny-goldin.com/blog/writing%20tips/]
* [http://evgeny-goldin.com/w/favicon.ico]
* [http://evgeny-goldin.com/w/images/c/cc/542x616xIStock_000011753901Small_cr.jpg.pagespeed.ic.rK68U-g7UD.jpg]
* [http://evgeny-goldin.com/w/opensearch_desc.php]
* [http://evgeny-goldin.com/w/skins/common/W.commonPrint.css,q207.pagespeed.cf.eppt5fGLM4.css]
* [http://evgeny-goldin.com/w/skins/common/W.shared.css,q207.pagespeed.cf.87u2PRUu_U.css]
* [http://evgeny-goldin.com/w/skins/common/ajax.js,q207.pagespeed.jm.8RZpzPHgVQ.js]
* [http://evgeny-goldin.com/w/skins/common/wikibits.js,q207.pagespeed.jm.2c6kmSlgcM.js]
* [http://evgeny-goldin.com/w/skins/monobook/W.main.css,q207.pagespeed.cf.v7HbZxNt1q.css]
* [http://evgeny-goldin.com/wiki/Brian_Tracy_-_10_Keys_to_A_More_Powerful_Personality]
* [http://evgeny-goldin.com/wiki/Continuous_Delivery]
* [http://evgeny-goldin.com/wiki/Evgeny_Goldin:About]
* [http://evgeny-goldin.com/wiki/Evgeny_Goldin:General_disclaimer]
* [http://evgeny-goldin.com/wiki/Evgeny_Goldin:Privacy_policy]
* [http://evgeny-goldin.com/wiki/File:IStock_000011753901Small_cr.jpg]
* [http://evgeny-goldin.com/wiki/GCommons]
* [http://evgeny-goldin.com/wiki/Getting_to_Yes:_Negotiating_Agreement_Without_Giving_In]
* [http://evgeny-goldin.com/wiki/Git]
* [http://evgeny-goldin.com/wiki/Gradle]
* [http://evgeny-goldin.com/wiki/Gradle_Plugins]
* [http://evgeny-goldin.com/wiki/Groovy]
* [http://evgeny-goldin.com/wiki/Maven_Plugins]
* [http://evgeny-goldin.com/wiki/MediaWiki_Tools]
* [http://evgeny-goldin.com/wiki/Spock]
* [http://evgeny-goldin.com/wiki/Spock_Extensions]
* [http://evgeny-goldin.com/wiki/Spring]
* [http://evgeny-goldin.com/wiki/TeamCity_Console]
* [http://evgeny-goldin.com/wiki/The_Pomodoro_Technique]
* [http://evgeny-goldin.com/wiki/Ubuntu]
* [http://github.com/evgeny-goldin/presentations]
* [http://twitter.com/evgeny_goldin]
* [http://www.amazon.com/Jenkins-Definitive-John-Ferguson-Smart/dp/1449305350/]
* [http://www.groovymag.com/]
* [http://www.mediawiki.org/]
* [http://www.methodsandtools.com/]
* [http://www.slideshare.net/evgenyg]

[http://evgeny-goldin.com/w/favicon.ico] - sending HEAD request ..
[http://evgeny-goldin.com/w/images/c/cc/542x616xIStock_000011753901Small_cr.jpg.pagespeed.ic.rK68U-g7UD.jpg] - sending HEAD request ..
[http://evgeny-goldin.com/] - sending HEAD request ..
[http://evgeny-goldin.com/blog/writing%20tips/] - sending HEAD request ..
[http://evgeny-goldin.com/blog/] - sending HEAD request ..
[http://evgeny-goldin.com/w/skins/common/W.commonPrint.css,q207.pagespeed.cf.eppt5fGLM4.css] - sending HEAD request ..
[http://evgeny-goldin.com/w/opensearch_desc.php] - sending HEAD request ..
[http://evgeny-goldin.com/blog/personal-productivity/] - sending HEAD request ..
[http://evgeny-goldin.com/w/favicon.ico] - can be read, [295] ms
[http://evgeny-goldin.com/w/skins/common/W.commonPrint.css,q207.pagespeed.cf.eppt5fGLM4.css] - can be read, [291] ms
[http://evgeny-goldin.com/w/images/c/cc/542x616xIStock_000011753901Small_cr.jpg.pagespeed.ic.rK68U-g7UD.jpg] - can be read, [295] ms
[http://evgeny-goldin.com/w/skins/common/W.shared.css,q207.pagespeed.cf.87u2PRUu_U.css] - sending HEAD request ..
[http://evgeny-goldin.com/w/skins/common/ajax.js,q207.pagespeed.jm.8RZpzPHgVQ.js] - sending HEAD request ..
[http://evgeny-goldin.com/w/skins/common/wikibits.js,q207.pagespeed.jm.2c6kmSlgcM.js] - sending HEAD request ..
[http://evgeny-goldin.com/w/skins/common/ajax.js,q207.pagespeed.jm.8RZpzPHgVQ.js] - can be read, [127] ms
[http://evgeny-goldin.com/w/skins/common/W.shared.css,q207.pagespeed.cf.87u2PRUu_U.css] - can be read, [127] ms
[http://evgeny-goldin.com/w/skins/monobook/W.main.css,q207.pagespeed.cf.v7HbZxNt1q.css] - sending HEAD request ..
[http://evgeny-goldin.com/w/skins/common/wikibits.js,q207.pagespeed.jm.2c6kmSlgcM.js] - can be read, [131] ms

...

[http://github.com/evgeny-goldin/gradle-plugins-test/blob/149fbbc50995d66d64902736ab1a53123d81ead1/build.gradle] - can be read, [230] ms
[http://confluence.jetbrains.com/display/TCD7/Development+Environment] - sending GET request ..
[http://confluence.jetbrains.com/display/TCD7/Development+Environment] - can be read, [266] ms
[http://evgeny-goldin.com/wiki/File:GitDump.png] - can be read, [2682] ms
##teamcity[progressMessage '1184 links processed, 1 broken, 0 queued']
##teamcity[progressMessage 'Writing report']


[1184] links processed in 126 sec, [553] Kb downloaded

[1] broken link found:


- [http://git-scm.com/2010/03/02/undoing-merges.html]

  Path:

  [http://evgeny-goldin.com/wiki]
  =>
  [http://evgeny-goldin.com/wiki/Main_Page]
  =>
  [http://evgeny-goldin.com/wiki/Git]
  =>
  [http://progit.org/2010/03/02/undoing-merges.html]
  =>
  [http://git-scm.com/2010/03/02/undoing-merges.html]

  Referred by [1] resource:

  [http://evgeny-goldin.com/wiki/Git]
</syntaxhighlight>